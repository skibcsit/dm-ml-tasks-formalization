package ru.mephi.csit.dmml.tasks.propsformalization

import cats.effect.{ExitCode, IO, IOApp}

object PropsTasksApp extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- PropsTasksF.writeFormalizationQuestions[IO]
      _ <- PropsTasksF.writeDeformalizationQuestions[IO]
      _ <- PropsTasksF.writeKR2Questions[IO]
      _ <- PropsTasksF.writeKR3Questions[IO]
    } yield ExitCode.Success

}
