package ru.mephi.csit.dmml.tasks.propsformalization

import java.util.Objects
import cats.instances.either._
import cats.syntax.either._
import cats.syntax.option._
import cats.instances.list._
import cats.syntax.traverse._

final class PropsDeformalization(
  textualizer: Textualizer,
  formulas: List[PropsExpr],
) {

  private def invert(expr: PropsExpr): Either[String, PropsExpr] =
    expr match {
      case Implication(left, right) => Implication(right, left).asRight
      case other => s"cannot invert ${other.formulaString}".asLeft
    }

  private def questionFrom(
    expr: PropsExpr,
    seed: Int,
  ): Either[String, Question[List[Answer]]] =
    for {
      rightTexts <- textualizer.textualize(expr)
      permutedRightTexts <-
        rightTexts.permutations
          .drop(seed % rightTexts.length)
          .nextOption()
          .toRight("dropped all right texts")
      rightDirectText <- permutedRightTexts
        .find(!_.contains("только"))
        .toRight("no rightDirectText")
      rightReverseText <- permutedRightTexts
        .find(_.contains("только"))
        .toRight("no rightReverseText")
      inverted      <- invert(expr)
      invertedTexts <- textualizer.textualize(inverted)
      permutedInvertedTexts <-
        invertedTexts.permutations
          .drop(seed.hashCode % invertedTexts.length)
          .nextOption()
          .toRight("dropped all inverted texts")
      invertedDirectText <- permutedInvertedTexts
        .find(!_.contains("только"))
        .toRight("no invertedDirectText")
      invertedReverseText <- permutedInvertedTexts
        .find(_.contains("только"))
        .toRight("no invertedReverseText")
      answers = List(
        Answer(false, s""""${rightDirectText.capitalize}."""", 50.some),
        Answer(false, s""""${rightReverseText.capitalize}"""", 50.some),
        Answer(false, s""""${invertedDirectText.capitalize}."""", -100.some),
        Answer(false, s""""${invertedReverseText.capitalize}."""", -100.some),
      )
      permutations = answers.permutations.toList
      permutedAnswers = permutations(seed % permutations.length)
    } yield
      Question(
        s"deformalization-${Objects.hash(expr, permutedAnswers)}",
        varDefsText +
          s"""Выберите ВСЕ высказывания, эквивалентные следующей формуле\\: """ +
          s"""${embedLatex(expr.latexString)}.""",
        permutedAnswers,
      )

  def questions: Either[String, List[Question[List[Answer]]]] =
    formulas.zipWithIndex
      .filter(_._2 % 9 == 0)
      .traverse { case (expr, index) => questionFrom(expr, index * 53) }

}
