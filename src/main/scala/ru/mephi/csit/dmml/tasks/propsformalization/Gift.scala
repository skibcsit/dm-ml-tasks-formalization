package ru.mephi.csit.dmml.tasks.propsformalization

import cats.Functor
import cats.instances.string._
import cats.syntax.option._
import simulacrum.typeclass

final case class IAnswer[TText](
  correct: Boolean,
  text: TText,
  percent: Option[Int] = None,
)

object IAnswer {
  implicit val iAnswerFunctor: Functor[IAnswer] =
    cats.derived.semiauto.functor
}

object Answer {
  def apply(
    correct: Boolean,
    text: String,
    percent: Option[Int] = None,
  ): Answer = IAnswer(correct, text, percent)
}

final case class Question[TAnswers](
  title: String,
  text: String,
  answers: TAnswers,
)

final case class CategoryQuestions[TAnswers](
  category: String,
  questions: List[Question[TAnswers]],
)

@typeclass trait TxtEncoder[T] {
  def asTxt(t: T): String
}

object TxtEncoder {

  import ops._

  implicit val answerTxtEecoder: TxtEncoder[Answer] =
    answer =>
      s"${if (answer.correct) "=" else "~"}" +
        answer.percent.map(percent => s"%$percent%").orEmpty +
        answer.text

  implicit val answersTxtEecoder: TxtEncoder[List[Answer]] =
    _.map(_.asTxt).mkString("\n")

  implicit def questionTxtEecoder[TAnswers: TxtEncoder]
    : TxtEncoder[Question[TAnswers]] =
    question => s"""::${question.title}
         |::${question.text}{
         |${question.answers.asTxt}
         |}""".stripMargin

  implicit def questionsTxtEecoder[TAnswers: TxtEncoder]
    : TxtEncoder[List[Question[TAnswers]]] =
    _.map(_.asTxt).mkString("\n\n")

  implicit def categoryQuestionsTxtEecoder[TAnswers: TxtEncoder]
    : TxtEncoder[CategoryQuestions[TAnswers]] =
    x => s"""$$CATEGORY: $$module$$/${x.category}
         |
         |${x.questions.asTxt}
         |""".stripMargin

  implicit val booleanTxtEncoder: TxtEncoder[Boolean] =
    if (_) "T" else "F"

}
