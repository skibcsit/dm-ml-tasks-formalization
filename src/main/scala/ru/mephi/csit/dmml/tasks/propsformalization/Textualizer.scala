package ru.mephi.csit.dmml.tasks.propsformalization

import cats.data.Nested
import cats.instances.either._
import cats.instances.list._
import cats.syntax.functor._

final case class Textualizer(texts: VarTextsEnv) {

  def textualize(expr: PropsExpr): Either[String, List[String]] =
    expr match {
      case v @ Var(_) => texts.get(v).map(_.positive :: Nil)
      case Negation(expr) =>
        expr match {
          case v @ Var(_) => texts.get(v).map(_.negative :: Nil)
          case other =>
            (
              for {
                otherText <- Nested(textualize(other))
              } yield s"неверно, что $otherText"
            ).value
        }
      case Implication(left, right) =>
        for {
          leftTexts  <- textualize(left)
          rightTexts <- textualize(right)
        } yield
          for {
            leftText  <- leftTexts
            rightText <- rightTexts
            result <-
              s"если $leftText, то $rightText" ::
                s"$rightText, если $leftText" ::
                s"$leftText, только если $rightText" ::
                s"только если $rightText, то $leftText" ::
                s"когда $leftText, тогда $rightText" ::
                s"$rightText тогда, когда $leftText" ::
                s"$leftText тогда, только когда $rightText" ::
                s"только когда $rightText, тогда $leftText" ::
                Nil
          } yield result
      case Conjunction(left, right) =>
        for {
          leftTexts  <- textualize(left)
          rightTexts <- textualize(right)
        } yield
          for {
            leftText  <- leftTexts
            rightText <- rightTexts
          } yield s"$leftText и $rightText"
      case Disjunction(left, right) =>
        for {
          leftTexts  <- textualize(left)
          rightTexts <- textualize(right)
        } yield
          for {
            leftText  <- leftTexts
            rightText <- rightTexts
          } yield s"$leftText или $rightText"
      case Xor(left, right) =>
        for {
          leftTexts  <- textualize(left)
          rightTexts <- textualize(right)
        } yield
          for {
            leftText  <- leftTexts
            rightText <- rightTexts
          } yield s"$leftText либо $rightText"
    }

}
