package ru.mephi.csit.dmml.tasks.propsformalization

import cats.instances.either._
import cats.instances.list._
import cats.syntax.traverse._

import java.util.Objects

final class PropsFormalization(
  textualizer: Textualizer,
  formulas: List[PropsExpr],
) {

  private def wrongExprs(expr: PropsExpr): List[PropsExpr] =
    expr match {
      case Implication(left, right) =>
        (for {
          wrongLeft  <- left :: wrongExprs(left)
          wrongRight <- right :: wrongExprs(right)
          wrong <-
            Implication(wrongLeft, wrongRight) ::
              Implication(wrongRight, wrongLeft) :: Nil
        } yield wrong).filter(_ != expr)
      case Conjunction(left, right) =>
        wrongCDX(left, right).filter(_ != expr)
      case Disjunction(left, right) =>
        wrongCDX(left, right).filter(_ != expr)
      case Xor(left, right) =>
        wrongCDX(left, right).filter(_ != expr)
      case _ => Nil
    }

  private def wrongCDX(left: PropsExpr, right: PropsExpr) =
    for {
      wrongLeft  <- left :: wrongExprs(left)
      wrongRight <- right :: wrongExprs(right)
      wrong <-
        Conjunction(wrongLeft, wrongRight) ::
          Disjunction(wrongLeft, wrongRight) ::
          Xor(wrongLeft, wrongRight) ::
          Nil
    } yield wrong

  private def questionsFrom(
    expr: PropsExpr,
    seed: Int,
  ): Either[String, List[Question[List[Answer]]]] =
    textualizer
      .textualize(expr).map { texts =>
        val filtered =
          texts.filter(str => str.contains("только") || str.contains("либо"))
        for {
          (text, index) <-
            filtered.zipWithIndex
              .filter(p => (seed + p._2) % filtered.length % 3 == 0)
          answers = Answer(true, embedLatex(expr.latexString)) ::
            wrongExprs(expr)
              .map(wrong => Answer(false, embedLatex(wrong.latexString)))
          permutations = answers.permutations.toList
          permutedAnswers = permutations((seed + index) % permutations.length)
        } yield
          Question[List[Answer]](
            s"form3-${Objects.hash(text, permutedAnswers)}",
            varDefsText +
              s"""Выберите верную формализацию следующего высказывания\\: """ +
              s""""${text.capitalize}."""",
            permutedAnswers,
          )
      }

  def questions: Either[String, List[Question[List[Answer]]]] =
    formulas.zipWithIndex
      .filter(_._2 % 20 == 0)
      .flatTraverse { case (expr, index) =>
        questionsFrom(expr, index * 100)
      }

}
