package ru.mephi.csit.dmml.tasks.propsformalization

sealed trait PropsExpr {
  def formulaString: String
  def latexString: LatexString
}

final case class Var(char: Char) extends PropsExpr {
  def formulaString: String = char.toString
  def latexString: LatexString = LatexString(char.toString)
}

final case class Negation(expr: PropsExpr) extends PropsExpr {
  def formulaString: String = s"¬${expr.formulaString}"
  def latexString: LatexString = LatexString(s"\\neg ${expr.latexString}")
}

final case class Implication(left: PropsExpr, right: PropsExpr)
    extends PropsExpr {
  def formulaString: String =
    s"(${left.formulaString} → ${right.formulaString})"
  def latexString: LatexString =
    LatexString(s"(${left.latexString} \\to ${right.latexString})")
}

final case class Conjunction(left: PropsExpr, right: PropsExpr)
    extends PropsExpr {
  def formulaString: String =
    s"(${left.formulaString} ∧ ${right.formulaString})"
  def latexString: LatexString =
    LatexString(s"(${left.latexString} \\wedge ${right.latexString})")
}

final case class Disjunction(left: PropsExpr, right: PropsExpr)
    extends PropsExpr {
  def formulaString: String =
    s"(${left.formulaString} ∨ ${right.formulaString})"
  def latexString: LatexString =
    LatexString(s"(${left.latexString} \\vee ${right.latexString})")
}

final case class Xor(left: PropsExpr, right: PropsExpr) extends PropsExpr {
  def formulaString: String =
    s"(${left.formulaString} ⊕ ${right.formulaString})"
  def latexString: LatexString =
    LatexString(s"(${left.latexString} \\oplus ${right.latexString})")
}
