package ru.mephi.csit.dmml.tasks.propsformalization

import cats.syntax.option._
import ru.mephi.csit.dmml.tasks.propsformalization.TxtEncoder.ops._

object KR3 {

  private val inSymbol = LatexString("\\in")
  private val subsetSymbol = LatexString("\\subseteq")

  private def task(
    str1: LatexString,
    str2: LatexString,
    answers: List[LatexString],
  ) =
    Question(
      s"sets-in-vs-subset-${str1.hashCode}",
      s"Какой из символов ${inSymbol.embed} или " +
        s"${subsetSymbol.embed} может быть вставлен между множествами " +
        s"${str1.embed} и ${str2.embed}," +
        s"чтобы получилось истинное утверждение?",
      Answer(
        false,
        inSymbol.embed,
        ((if (answers.contains(inSymbol)) 1
          else -1) * 100 / answers.length).some,
      ) ::
        Answer(
          false,
          subsetSymbol.embed,
          ((if (answers.contains(subsetSymbol)) 1
            else -1) * 100 / answers.length).some,
        ) ::
        Nil,
    )

  def txts: List[String] =
    CategoryQuestions(
      "$module$/dm-ml/sets/in-vs-subset",
      task(
        LatexString("""\{\{2,3\}\}"""),
        LatexString("""\{ 1, \{2,3\}, 20\}"""),
        subsetSymbol :: Nil,
      ) ::
        task(
          LatexString("""\{1,2\}"""),
          LatexString("""\{1,\{ 1, 2\}, 2\}"""),
          subsetSymbol :: inSymbol :: Nil,
        ) ::
        task(
          LatexString("""\varnothing"""),
          LatexString("""\{\{\varnothing\} , 1, 2, \{1\}\}"""),
          subsetSymbol :: Nil,
        ) ::
        task(
          LatexString("""\{\varnothing\}"""),
          LatexString("""\{\{\varnothing\} , 1, 2, \{1\}\}"""),
          inSymbol :: Nil,
        ) ::
        task(
          LatexString("""\left\{\left\{1, 2\right\}\right\}"""),
          LatexString("""\left\{\{1,2\}, 1, 2\right\}"""),
          subsetSymbol :: Nil,
        ) ::
        task(
          LatexString("""\{\varnothing\}"""),
          LatexString("""\left\{\left\{\varnothing\right\}\right\}"""),
          inSymbol :: Nil,
        ) ::
        Nil,
    ).asTxt ::
      Nil

}
