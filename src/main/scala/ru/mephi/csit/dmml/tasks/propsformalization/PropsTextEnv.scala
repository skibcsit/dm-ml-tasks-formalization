package ru.mephi.csit.dmml.tasks.propsformalization

final case class VarTextsEnv(texts: Map[Var, VarTexts]) {
  def get(v: Var): Either[String, VarTexts] =
    texts.get(v).toRight(s"No texts for $v")
}

final case class VarTexts(positive: String, negative: String)
