package ru.mephi.csit.dmml.tasks.propsformalization

import cats.Applicative
import cats.effect.Sync
import cats.syntax.flatMap._
import cats.syntax.functor._
import ru.mephi.csit.dmml.tasks.propsformalization.TxtEncoder.ops._
import shapeless.HNil

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, Paths}

object PropsTasksF {

  private val textualizer: Textualizer =
    Textualizer(
      VarTextsEnv(
        Map(
          Var('a') -> VarTexts("поезд A прибудет вовремя", "поезд A опоздает"),
          Var('b') -> VarTexts("поезд B прибудет вовремя", "поезд B опоздает"),
          Var('c') -> VarTexts("поезд C прибудет вовремя", "поезд C опоздает"),
        ),
      ),
    )

  private def bins(x: PropsExpr, y: PropsExpr): List[PropsExpr] =
    Conjunction(x, y) ::
      Disjunction(x, y) ::
      Xor(x, y) ::
      Nil

  private val formulas: List[PropsExpr] =
    for {
      vars <- (Var('a') :: Var('b') :: Var('c') :: HNil).permutations.toList
      (vx, vy, vz) = vars.tupled
      x    <- vx :: Negation(vx) :: Nil
      y    <- vy :: Negation(vy) :: Nil
      z    <- vz :: Negation(vz) :: Nil
      xy   <- bins(x, y)
      yz   <- bins(y, z)
      expr <- Implication(xy, z) :: Implication(x, yz) :: Nil
    } yield expr

  private def initTargetGiftPath[F[_]: Sync](path: String): F[Path] =
    Applicative[F]
      .pure(Paths.get(s"target/gift/$path"))
      .flatTap(result => Sync[F].delay(result.getParent.toFile.mkdirs()))

  def writeFormalizationQuestions[F[_]: Sync]: F[Unit] =
    for {
      questions <- Sync[F].fromEither(
        new PropsFormalization(textualizer, formulas).questions.left
          .map(new Exception(_)),
      )
      path <- initTargetGiftPath("formalization.txt")
      _ <- Sync[F].delay(
        Files.write(
          path,
          CategoryQuestions("dm-ml/props/formalization-rnd", questions).asTxt
            .getBytes(StandardCharsets.UTF_8),
        ),
      )
    } yield ()

  def writeDeformalizationQuestions[F[_]: Sync]: F[Unit] =
    for {
      questions <- Sync[F].fromEither(
        new PropsDeformalization(textualizer, formulas).questions.left
          .map(new Exception(_)),
      )
      path <- initTargetGiftPath("deformalization.txt")
      _ <- Sync[F].delay(
        Files.write(
          path,
          CategoryQuestions("dm-ml/props/deformalization-rnd", questions).asTxt
            .getBytes(StandardCharsets.UTF_8),
        ),
      )
    } yield ()

  def writeKR2Questions[F[_]: Sync]: F[Unit] =
    for {
      path <- initTargetGiftPath("kr-2.txt")
      _ <- Sync[F].delay(
        Files.write(
          path,
          KR2.txts.mkString("\n").getBytes(StandardCharsets.UTF_8),
        ),
      )
    } yield ()

  def writeKR3Questions[F[_]: Sync]: F[Unit] =
    for {
      path <- initTargetGiftPath("kr-3.txt")
      _ <- Sync[F].delay(
        Files.write(
          path,
          KR3.txts.mkString("\n").getBytes(StandardCharsets.UTF_8),
        ),
      )
    } yield ()

}
