package ru.mephi.csit.dmml.tasks.propsformalization

import cats.syntax.option._
import cats.syntax.functor._

import TxtEncoder.ops._

object KR2 {

  private def formalizationtask(str1: List[LatexString], str2: String): String =
    s"Пусть ${str1.map(_.embed).mkString(", ")}. Отметить все верные формализации следующего высказывания: «$str2»."

  private val prational =
    LatexString("""Q(x) = \text{«}x\text{ — рациональное число»}""")

  private def preal =
    LatexString("""R(x)=\text{«}x\text{ — действительное число»}""")

  private def pprime =
    LatexString("""P(x)=\text{«}x\text{ — простое число»}""")

  private def pcardinal =
    LatexString("""C(x) = \text{«}x\text{ — целое число»}""")

  private def pless =
    LatexString("""L(x,y) = \text{«}x < y\text{»}""")

  private def pdivisor =
    LatexString(
      """D(x,y) = \text{«}y\text{ — натуральный делитель }x\text{»}""",
    )

  private def pequal =
    LatexString("""E(x,y) = \text{«}x = y\text{»}""")

  private def formalizationQuestion(
    str1: List[LatexString],
    str2: String,
    answers: List[IAnswer[LatexString]],
  ): Question[List[Answer]] =
    Question(
      s"kr-formalizationtask-${(str1, str2).hashCode}",
      formalizationtask(str1, str2),
      answers.map(_.map(_.embed)),
    )

  private def formalizeDouble(
    str1: String,
    answers: List[IAnswer[LatexString]],
  ): Question[List[Answer]] =
    Question(
      s"kr-taskzero-${str1.hashCode}",
      formalizationtask(prational :: preal :: Nil, str1),
      answers.map(_.map(_.embed)),
    )

  private def formalizeSingle(
    str1: String,
    answers: List[IAnswer[LatexString]],
  ): Question[List[Answer]] =
    Question(
      s"kr-formalizeSingle-${str1.hashCode}",
      formalizationtask(
        LatexString(
          """R(x,y,z) = \text{«Преподаватель }x\text{ ведет занятия по дисциплине }y\text{ в группе }z\text{»}""",
        ) :: Nil,
        str1,
      ),
      answers.map(_.map(_.embed)),
    )

  private def tasktwo(
    str1: LatexString,
    str2: String,
    answers: Boolean,
  ): Question[Boolean] =
    Question(
      s"kr-tasktwo-${(str1, str2).hashCode}",
      s"Истинно ли высказывание ${str1.embed} при интерпретации на множестве всех $str2 чисел?",
      answers,
    )

  private def tasksix(
    str1: LatexString,
    answers: List[IAnswer[LatexString]],
  ): Question[List[Answer]] =
    Question(
      s"kr-tasksix-${str1.embed.hashCode}",
      s"Выбрать наиболее краткую предваренную нормальную форму " +
        s"для следующей формулы ${str1.embed}",
      answers.map(_.map(_.embed)),
    )

  private def varFindQuestion(
    formula: LatexString,
    varType: String,
    vars: List[String],
  ): Question[List[Answer]] =
    Question(
      s"kr-var-find-${(formula.embed, varType).hashCode}",
      s"Отметьте все $varType переменные следующей формулы: " + formula.embed,
      ("x" :: "y" :: "z" :: "P" :: "Q" :: "\\exists" :: "\\forall" :: Nil)
        .map(v =>
          Answer(
            false,
            LatexString(v).embed,
            ((if (vars.contains(v)) 1 else -1) * 100 / vars.length).some,
          ),
        ),
    )

  private def propCheckQuestion(
    formula: LatexString,
    answer: Boolean,
  ): Question[Boolean] =
    Question(
      s"kr-prop-check-${formula.embed.hashCode}",
      s"Является ли следующая формула логики предикатов переменным высказыванием, " +
        s"не зависящим от значений свободных предметных переменных: " +
        formula.embed,
      answer,
    )

  private def tautCheckQuestion(
    formula: LatexString,
    answer: Boolean,
  ): Question[Boolean] =
    Question(
      s"kr-taut-check-${formula.embed.hashCode}",
      s"Является ли формула общезначимой: " + formula.embed,
      answer,
    )

  /**
   * for IDEA to not complain about prefix `-` in -int.some
   */
  private def neg(int: Int) = -int

  def txts: List[String] =
    CategoryQuestions(
      "$module$/dm-ml/predicates/kr-formalize-single",
      formalizeSingle(
        "Неверно, что существует дисциплина, которую ведет каждый преподаватель в каждой группе",
        IAnswer(
          false,
          LatexString("""\overline{\exists y \forall x \forall z R(x,y,z)}"""),
          50.some,
        ) ::
          IAnswer(
            false,
            LatexString(
              """\forall y \exists x \exists z \overline{R(x,y,z)}""",
            ),
            50.some,
          ) ::
          IAnswer(
            false,
            LatexString(
              """\exists y \forall x \forall z \overline{R(x,y,z)}""",
            ),
            neg(50).some,
          ) ::
          IAnswer(
            false,
            LatexString(
              """\overline{\forall y \exists x \exists z R(x,y,z)}""",
            ),
            neg(50).some,
          ) ::
          Nil,
      ) ::
        formalizeSingle(
          "Для каждой группы найдется дисциплина, " +
            "для которой не существует преподавателя, который её ведет",
          IAnswer(
            false,
            LatexString(
              """\forall x \exists y \forall z \overline{R(x,y,z)}""",
            ),
            50.some,
          ) ::
            IAnswer(
              false,
              LatexString(
                """\overline{\exists x \forall y \exists z R(x,y,z)}""",
              ),
              50.some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\overline{\forall x \exists y \forall z R(x,y,z)}""",
              ),
              neg(50).some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\exists x \forall y \exists z \overline{R(x,y,z)}""",
              ),
              neg(50).some,
            ) ::
            Nil,
        ) ::
        formalizeSingle(
          "Для каждого преподавателя неверно, что существует группа, в которой он ведет занятия по всем её (группы) дисциплинам",
          IAnswer(
            false,
            LatexString(
              """\overline{\exists x \exists y \forall z R(x,y,z)}""",
            ),
            50.some,
          ) ::
            IAnswer(
              false,
              LatexString(
                """\forall x \forall y \exists z \overline{R(x,y,z)}""",
              ),
              50.some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\exists x \exists y \forall z \overline{R(x,y,z)}""",
              ),
              neg(50).some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\overline{\forall x \forall y \exists z R(x,y,z)}""",
              ),
              neg(50).some,
            ) ::
            Nil,
        ) ::
        Nil,
    ).asTxt ::
      CategoryQuestions(
        "$module$/dm-ml/predicates/kr-formalize-double",
        formalizeDouble(
          "Все рациональные числа являются действительными",
          IAnswer(
            false,
            LatexString("""\forall x (Q(x) \to R(x))"""),
            50.some,
          ) ::
            IAnswer(
              false,
              LatexString("""\forall x (Q(x) \wedge R(x))"""),
              neg(50).some,
            ) ::
            IAnswer(
              false,
              LatexString("""\forall x (Q(x) \leftrightarrow R(x))"""),
              neg(50).some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\overline{\exists x (Q(x) \wedge \overline{R(x)})}""",
              ),
              50.some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\overline{\exists x (\overline{Q(x)} \vee \overline{R(x)})}""",
              ),
              neg(50).some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\overline{\exists x (Q(x) \nleftrightarrow R(x))}""",
              ),
              neg(50).some,
            ) ::
            Nil,
        ) ::
          formalizeDouble(
            "Некоторые действительные числа являются рациональными",
            IAnswer(
              false,
              LatexString("""\exists x (Q(x) \wedge R(x))"""),
              50.some,
            ) ::
              IAnswer(
                false,
                LatexString("""\exists x (Q(x) \to R(x))"""),
                neg(50).some,
              ) ::
              IAnswer(
                false,
                LatexString("""\exists x (Q(x) \leftrightarrow R(x))"""),
                neg(50).some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\overline{\forall x (\overline{Q(x)} \vee \overline{R(x)})}""",
                ),
                50.some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\overline{\forall x (Q(x) \wedge \overline{R(x)})}""",
                ),
                neg(50).some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\overline{\forall x (Q(x) \nleftrightarrow R(x))}""",
                ),
                neg(50).some,
              ) ::
              Nil,
          ) ::
          formalizeDouble(
            "Не каждое действительное число является рациональным",
            IAnswer(
              false,
              LatexString("""\exists x (R(x) \wedge \overline{Q(x)})"""),
              50.some,
            ) ::
              IAnswer(
                false,
                LatexString("""\exists x (R(x) \to \overline{Q(x)})"""),
                neg(50).some,
              ) ::
              IAnswer(
                false,
                LatexString("""\exists x (R(x) \nleftrightarrow Q(x))"""),
                neg(50).some,
              ) ::
              IAnswer(
                false,
                LatexString("""\overline{\forall x (R(x) \to Q(x))}"""),
                50.some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\overline{\forall x (R(x) \wedge \overline{Q(x)})}""",
                ),
                neg(50).some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\overline{\forall x (R(x) \leftrightarrow Q(x))}""",
                ),
                neg(50).some,
              ) ::
              Nil,
          ) ::
          Nil,
      ).asTxt ::
      CategoryQuestions(
        "$module$/dm-ml/predicates/kr-formalizationtask",
        formalizationQuestion(
          pprime :: pcardinal :: pless :: pdivisor :: Nil,
          """Каждое целое, большее 1, имеет простой делитель""",
          IAnswer(
            false,
            LatexString(
              """\forall x \exists y (C(x) \wedge L(1, x) \to D(x, y) \wedge P(y))""",
            ),
            100.some,
          ) ::
            IAnswer(
              false,
              LatexString(
                """\forall x \forall y (C(x) \wedge L(1, x) \to D(x, y) \wedge P(y))""",
              ),
              neg(100).some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\forall x \exists y (C(x) \wedge L(1, x) \wedge D(x, y) \wedge P(y))""",
              ),
              neg(100).some,
            ) ::
            IAnswer(
              false,
              LatexString(
                """\forall x \exists y (C(x) \wedge L(1, x) \wedge D(x, y) \to P(y))""",
              ),
              neg(100).some,
            ) ::
            Nil,
        ) ::
          formalizationQuestion(
            pprime :: pdivisor :: pequal :: Nil,
            """Все делители простых чисел равны самому числу или 1""",
            IAnswer(
              false,
              LatexString(
                """\forall x \forall y (P(x) \wedge D(x, y) \to E(x, y) \vee E(1, y))""",
              ),
              100.some,
            ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall x \exists y (P(x) \wedge D(x, y) \to E(x, y) \vee E(1, y))""",
                ),
                neg(100).some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall x \forall y (P(x) \wedge D(x, y) \wedge (E(x, y) \vee E(1, y)))""",
                ),
                neg(100).some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall x \forall y (P(x) \wedge D(x, y) \wedge (E(x, y) \to E(1, y)))""",
                ),
                neg(100).some,
              ) ::
              Nil,
          ) ::
          formalizationQuestion(
            pprime :: pcardinal :: pless :: Nil,
            s"""Для каждого целого числа ${LatexString(
                "n",
              ).embed} найдется простое число, большее ${LatexString(
                "n",
              ).embed}""",
            IAnswer(
              false,
              LatexString(
                """\forall n \exists y (C(n) \to P(y) \wedge L(n, y))""",
              ),
              100.some,
            ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall n \forall y (C(n) \to P(y) \wedge L(n, y))""",
                ),
                neg(100).some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall n \exists y (C(n) \wedge P(y) \wedge L(n, y))""",
                ),
                neg(100).some,
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall n \exists y (C(n) \wedge P(y) \to L(n, y))""",
                ),
                neg(100).some,
              ) ::
              Nil,
          ) ::
          Nil,
      ).asTxt ::
      CategoryQuestions(
        "$module$/dm-ml/predicates/kr-tasktwo",
        tasktwo(
          LatexString("""\forall y \exists x \; (x < y)"""),
          """отрицательных целых""",
          true,
        ) ::
          tasktwo(
            LatexString("""\forall y \exists x \; (x < y)"""),
            """натуральных""",
            false,
          ) ::
          tasktwo(
            LatexString("""\exists x \exists y \; (x\;mod\;y = 0)"""),
            """четных""",
            true,
          ) ::
          Nil,
      ).asTxt ::
      CategoryQuestions(
        "$module$/dm-ml/predicates/kr-tasksix",
        tasksix(
          LatexString("""\forall x P(x) \to \forall x Q(x)"""),
          IAnswer(
            false,
            LatexString("""\forall a (\overline{P(a)} \wedge Q(a))"""),
          ) ::
            IAnswer(
              false,
              LatexString("""\exists a (\overline{P(a)} \wedge Q(a))"""),
            ) ::
            IAnswer(
              false,
              LatexString(
                """\forall a \forall b (\overline{P(a)} \wedge Q(b))""",
              ),
            ) ::
            IAnswer(
              false,
              LatexString(
                """\forall a \exists b (\overline{P(a)} \wedge Q(b))""",
              ),
            ) ::
            IAnswer(
              true,
              LatexString(
                """\exists a \forall b (\overline{P(a)} \wedge Q(b))""",
              ),
            ) ::
            IAnswer(
              false,
              LatexString(
                """\exists a \exists b (\overline{P(a)} \wedge Q(b))""",
              ),
            ) ::
            Nil,
        ) ::
          tasksix(
            LatexString("""\forall x P(x) \to \exists x Q(x)"""),
            IAnswer(
              false,
              LatexString("""\forall a (\overline{P(a)} \wedge Q(a))"""),
            ) ::
              IAnswer(
                true,
                LatexString("""\exists a (\overline{P(a)} \wedge Q(a))"""),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall a \forall b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall a \exists b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\exists a \forall b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\exists a \exists b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              Nil,
          ) ::
          tasksix(
            LatexString("""\exists x P(x) \to \forall x Q(x)"""),
            IAnswer(
              false,
              LatexString("""\forall a (\overline{P(a)} \wedge Q(a))"""),
            ) ::
              IAnswer(
                false,
                LatexString("""\exists a (\overline{P(a)} \wedge Q(a))"""),
              ) ::
              IAnswer(
                true,
                LatexString(
                  """\forall a \forall b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall a \exists b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\exists a \forall b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\exists a \exists b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              Nil,
          ) ::
          tasksix(
            LatexString("""\exists x P(x) \to \exists x Q(x)"""),
            IAnswer(
              false,
              LatexString("""\forall a (\overline{P(a)} \wedge Q(a))"""),
            ) ::
              IAnswer(
                false,
                LatexString("""\exists a (\overline{P(a)} \wedge Q(a))"""),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\forall a \forall b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                true,
                LatexString(
                  """\forall a \exists b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\exists a \forall b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              IAnswer(
                false,
                LatexString(
                  """\exists a \exists b (\overline{P(a)} \wedge Q(b))""",
                ),
              ) ::
              Nil,
          ) ::
          Nil,
      ).asTxt ::
      CategoryQuestions(
        "$module$/dm-ml/predicates/kr-var-find",
        varFindQuestion(
          LatexString("""\forall x \exists y (P(x, y) \to Q(z))"""),
          "свободные предметные",
          "z" :: Nil,
        ) ::
          varFindQuestion(
            LatexString("""\forall x \exists y (P(x, y) \to Q(z))"""),
            "связанные предметные",
            "x" :: "y" :: Nil,
          ) ::
          varFindQuestion(
            LatexString("""\forall x \exists y (P(x, y) \to Q(z))"""),
            "предикатные",
            "P" :: "Q" :: Nil,
          ) ::
          varFindQuestion(
            LatexString("""\forall x (P(x, y) \to \exists z Q(z))"""),
            "свободные предметные",
            "y" :: Nil,
          ) ::
          varFindQuestion(
            LatexString("""\forall x (P(x, y) \to \exists z Q(z))"""),
            "связанные предметные",
            "x" :: "z" :: Nil,
          ) ::
          varFindQuestion(
            LatexString("""\forall x (P(x, y) \to \exists z Q(z))"""),
            "предикатные",
            "P" :: "Q" :: Nil,
          ) ::
          Nil,
      ).asTxt ::
      CategoryQuestions(
        "$module$/dm-ml/predicates/kr-prop-check",
        propCheckQuestion(
          LatexString("""P(x, y) \to Q(z)"""),
          false,
        ) ::
          propCheckQuestion(
            LatexString("""\forall x \exists y \forall z (P(x, y) \to Q(z))"""),
            true,
          ) ::
          propCheckQuestion(
            LatexString("""\forall x \exists y P(x, y) \to \forall z Q(z)"""),
            true,
          ) ::
          propCheckQuestion(
            LatexString("""\forall x \exists y P(x, y) \to \exists y Q(z)"""),
            false,
          ) ::
          propCheckQuestion(
            LatexString("""\forall x \exists y P(x, y) \to Q(z)"""),
            false,
          ) ::
          propCheckQuestion(
            LatexString("""\forall x P(x, y) \to \forall z Q(z)"""),
            false,
          ) ::
          Nil,
      ).asTxt ::
      CategoryQuestions(
        "$module$/dm-ml/predicates/kr-taut-check",
        tautCheckQuestion(
          LatexString(
            """\forall x (P(x) \vee Q(x)) \to (\forall x P(x) \vee \forall x Q(x))""",
          ),
          false,
        ) ::
          tautCheckQuestion(
            LatexString(
              """(\forall x P(x) \vee \forall x Q(x)) \to \forall x (P(x) \vee Q(x))""",
            ),
            true,
          ) ::
          tautCheckQuestion(
            LatexString(
              """\exists x (P(x) \wedge Q(x)) \to (\exists x P(x) \wedge \exists x Q(x))""",
            ),
            true,
          ) ::
          tautCheckQuestion(
            LatexString(
              """(\exists x P(x) \wedge \exists x Q(x)) \to \exists x (P(x) \wedge Q(x))""",
            ),
            false,
          ) ::
          Nil,
      ).asTxt ::
      Nil

}
