package ru.mephi.csit.dmml.tasks

package object propsformalization {

  type Answer = IAnswer[String]

  def embedLatex(string: LatexString): String =
    string.embed

  val varDefsText: String =
    s"""Пусть высказывание ${embedLatex(Var('x').latexString)} """ +
      s"""понимается как "поезд X прибудет вовремя", """ +
      s"""а ${embedLatex(Negation(Var('x')).latexString)} """ +
      s"""как "поезд X опоздает". """

}
