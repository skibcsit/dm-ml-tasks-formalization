package ru.mephi.csit.dmml.tasks.propsformalization

final case class LatexString(latex: String) {
  def embed: String =
    s"\\\\(${latex.replace("\\", "\\\\").replace("{", "\\{").replace("}", "\\}")}\\\\)"
}
