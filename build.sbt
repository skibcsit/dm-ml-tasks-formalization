scalaVersion := "2.13.10"

organization := "ru.mephi.csit"
name := "dm-ml-tasks-props-formalization"

libraryDependencies ++= Seq(
  "org.typelevel" %% "simulacrum" % "1.0.1",
  "org.typelevel" %% "cats-effect" % "3.5.7" ,
  "com.chuusai" %% "shapeless" % "2.3.12",
  "com.github.scopt" %% "scopt" % "4.1.0",
  "org.typelevel" %% "kittens" % "3.4.0",
)

scalacOptions ++= Seq(
  "-Xlint:-byname-implicit,_",
  "-feature",
  "-deprecation",
  "-Ymacro-annotations",
)

scalacOptions ++= (
  if (insideCI.value) Seq("-Xfatal-warnings")
  else                Seq()
)

scalafmtOnCompile := !insideCI.value
